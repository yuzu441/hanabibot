# 花火たんbot

システム開発2015用 slackbot

## style guide

[Cookpad CoffeeScript Coding Style Guide](https://github.com/cookpad/styleguide/blob/master/coffeescript.ja.md)

## ビルドスクリプト

bin/hubot - 通常の起動。デバッグなどに  
bin/start_hubot - プロセスを永続化  
bin/stop_hubot - 永続化しているプロセスを停止させる

## 依存関係

```
npm install -g coffee-script hubot forever
```

## API

### respond

**status**  
名前を返す

**雨降る？**  
(地域) 雨降る？  
と聞くとその地域の降水確率を返してくれる

### hear
