# Description:
#   saikawa inosuke photo
#
# Commands:
#   水瀬いのり - inosuke picture url


module.exports = (robot) ->

  robot.hear(/いのすけ|水瀬いのり/i, (msg) ->
    msg.http('http://cat-me.herokuapp.com/inosuke')
    .get() (err, res, body) ->
      msg.send JSON.parse(body).url
  )