# Description:
#   Weather info
#
# Commands:
#   hubot <area> 雨降る? - select area weather info


xpath = require('xpath')
dom = require('xmldom').DOMParser
_ = require('lodash')

areaCodeHash = {
  '大阪': 27
  '兵庫': 28
  '京都': 26
  '滋賀': 25
  '奈良': 29
  '和歌山': 30
}

module.exports = (robot) ->
  robot.respond /(\S+)\s?雨降る[\？|\?]?/i, (msg) ->

    if !areaCodeHash.hasOwnProperty(msg.match[1])
      msg.send 'どこの田舎ですか？'
      return

    # 地域が見つかった場合
    apiUrl = "http://www.drk7.jp/weather/xml/#{areaCodeHash[msg.match[1]]}.xml"
    msg.http(apiUrl)
    .get() (err, res, body) ->
      doc = new dom().parseFromString(body)
      xpathString = "//rainfallchance[1]/period"
      nodes = xpath.select(xpathString, doc)

      weatherList = _.map(nodes, (element) ->
        time = element.attributes[0].value
        probability = element.firstChild.toString()
        return new WeatherReport(time, probability)
      )

      weatherText = _.reduce(weatherList, (prev, current) ->
        return prev + current
      )

      msg.send("#{msg.match[1]}\n#{weatherText}")

class WeatherReport
  constructor: (@hour, @percent) ->

  weatherIcon: ->
    per = @percent
    if per < 30
      return ':sunny:'
    else if per < 50
      return ':cloud:'
    else
      return ':umbrella:'

  toString: () ->
    icon = @weatherIcon()
    return "#{@hour} #{@percent}% #{icon}\n"