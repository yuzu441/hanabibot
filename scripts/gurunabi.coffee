# Description:
#   gurunabiって打つとランダムで上本町周辺の飲食店のurlを返してくれるでゲソ
#   住所は任意でいいでゲソ
#   デフォルトは上本町でゲソ
# Commands:
#   hubot gurunabi カテゴリー [住所]

xpath = require('xpath')
dom = require('xmldom').DOMParser
_ = require('lodash')
API_CATEGORY_URL = "http://api.gnavi.co.jp/master/CategorySmallSearchAPI/20150630/?"
API_RESTAURANT_URL = "http://api.gnavi.co.jp/RestSearchAPI/20150630/?"
KEY = process.env.GURUNABI_API_KEY
DEFAULT_ADDRESS = "上本町"
HIT_PER_PAGE = 50 #1ページ辺りの表示数
req_category = "#{API_CATEGORY_URL}keyid=#{KEY}&format=xml"

module.exports = (robot) ->
  robot.respond /gurunabi (\S*) ?(\S*)$/i, (msg) ->
    name = msg.match[1]
    address = msg.match[2] || DEFAULT_ADDRESS
    offSet_Page = 1 #何ページ目を表示するか
    msg.http(req_category)
    .get() (err, res, body) ->
      doc = new dom().parseFromString(body)
      category = new Category(doc)
      food_division = category.search_name(name)
      req_restaurant = "#{API_RESTAURANT_URL}keyid=#{KEY}&hit_per_page=#{HIT_PER_PAGE}&offset_page=#{offSet_Page}&address=#{encodeURIComponent(address)}&format=xml&category_s=#{food_division}"
      msg.http(req_restaurant)
      .get() (err, res, body) ->
        doc = new dom().parseFromString(body)
        data = new ResponseData(doc)
        msg.send(data.getUrl())

class ResponseData
  constructor: (@doc) ->
    restaurant_list = xpath.select("//rest", @doc)
    order = Math.floor(Math.random() * restaurant_list.length) #表示する店の選択
    @restaurant_xpath = "//rest[@order=#{order}]"
  getName: ->
    return xpath.select("#{@restaurant_xpath}/name/text()", @doc).toString()
  getUrl: ->
    return xpath.select("#{@restaurant_xpath}/url/text()", @doc).toString()

class Category
  constructor: (@doc) ->
    @category_data = []
    elements =  xpath.select("//response/category_s", @doc)
    elements.forEach((v, i) =>
      xpath_select = "\//response/category_s[#{i}]/"
      category = {}
      category.name = xpath.select("#{xpath_select}category_s_name/text()", @doc).toString()
      category.code = xpath.select("#{xpath_select}category_s_code/text()", @doc).toString()
      @category_data.push(category)
    )

  search_name: (category_name) ->
    category = _.find(@category_data, (category_obj) ->
      return category_obj.name.match(category_name)
    )
    return category.code
