# Description:
#   dice info
#
# Commands:
#   <num>D<num>

module.exports = (robot) ->
  robot.respond /(\d+)D(\d+)/i, (msg) ->
    dice_count = msg.match[1]
    dice_range = msg.match[2]
    result = 0
    [1..dice_count].forEach(()->
      result += Math.floor(Math.random() * dice_range) + 1
    )

    msg.send(result.toString())

