# Description:
#   BOTの情報などを扱う
#
# Commands:
#   hubot status - Send bot status
#   hubot reboot - Bot process reboot

module.exports = (robot) ->

  robot.send({room: "botcreater"}, "ふぁ〜おはよ")

  # botのstatus表示したいけど、とりあえず名前のみ
  robot.respond /status/i, (msg) ->
    msg.send '花火だよー'

  robot.respond /reboot/i, (msg) ->
    owner = process.env.HUBOT_OWNER || ''
    sendUser = msg.message.user.name

    if owner == ''
      msg.send "@#{sendUser}: HUBOT_OWNERが設定されてないよ"

    else if owner != sendUser
      msg.send "@#{sendUser}: 再起動する権限がないよ"

    else if owner == sendUser
      msg.send "@#{sendUser}: 再起動するよ！"
      process.exit()
