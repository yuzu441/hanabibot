# Description:
#   「おなかすいた」などに反応して飯テロする
#
# Commands:
#   hubot /おなかすいた|はらへ/

API_URL = "https://ajax.googleapis.com/ajax/services/search/images"

module.exports = (robot) ->
  robot.hear /おなかすいた|はらへ/i, (msg) ->
    bomb = Math.random() * 10   # 毎回出ると鬱陶しいのでたまに出るように
    if bomb < 3
      hungryBomb(msg, (url) ->
        msg.send("@#{msg.message.user.name}: #{url}")
      )

hungryBomb = (msg, cb) ->
  serachWord = '飯テロ'
  searchPageNum = 8;
  searchStartPage = Math.floor(Math.random() * searchPageNum) + 1
  query = {
    v: '1.0'                  # APIのバージョン(1.0のみ使える模様)
    rsz: 'large'              # 取得する検索結果の数(small(4個)かlarge(8個)のみ)
    start: searchStartPage    # 表示する検索結果ページの指定
    q: serachWord             # 検索ワード
    safe: 'active'            # セーフサーチの有無
  }
  msg.http(API_URL)
    .query(query)
    .get() (err, res, body) ->
      searchResultObj = JSON.parse(body)
      resultObjArray = searchResultObj.responseData?.results
      if resultObjArray?.length > 0
        image = msg.random(resultObjArray)
        cb(ensureImageExtension(image.unescapedUrl))

ensureImageExtension = (url) ->
  urlExtensionReg = new RegExp("(png|jpe?g|gif)$", 'i')
  if url.match(urlExtensionReg)
    return url
  else
    return "#{url}.png"
